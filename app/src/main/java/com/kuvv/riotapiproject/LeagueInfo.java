package com.kuvv.riotapiproject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kuvv on 17.07.2017.
 */

public class LeagueInfo {
    @SerializedName("queueType")
    private String queueType;
    @SerializedName("playerOrTeamName")
    private String playerOrTeamName;
    @SerializedName("rank")
    private String rank;
    @SerializedName("tier")
    private String tier;
    @SerializedName("leaguePoints")
    private int leaguePoints;
    @SerializedName("wins")
    private int wins;
    @SerializedName("losses")
    private int losses;


    public String getQueueType() {
        return queueType;
    }

    public void setQueueType(String queueType) {
        this.queueType = queueType;
    }

    public String getPlayerOrTeamName() {
        return playerOrTeamName;
    }

    public void setPlayerOrTeamName(String playerOrTeamName) {
        this.playerOrTeamName = playerOrTeamName;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public int getLeaguePoints() {
        return leaguePoints;
    }

    public void setLeaguePoints(int leaguePoints) {
        this.leaguePoints = leaguePoints;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public LeagueInfo() {
    }
}



