package com.kuvv.riotapiproject;

import android.provider.BaseColumns;

/**
 * Created by kuvv on 05.08.2017.
 */

public class  PlayersContract {

    private PlayersContract() {}

    public static final String TABLE_NAME = "players";

    public static class Columns implements BaseColumns {

        public static final String PLAYER_NAME = "name";
        public static final String PLAYER_ID = "id";

        private Columns(){

        }
    }
}
