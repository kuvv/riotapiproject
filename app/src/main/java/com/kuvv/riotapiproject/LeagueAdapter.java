package com.kuvv.riotapiproject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kuvv on 20.07.2017.
 */

public class LeagueAdapter extends RecyclerView.Adapter<LeagueAdapter.MyViewHolder> {
    private List<LeagueInfo> leagueInfoList;
    public OnPlayerDeleted onPlayerDeleted;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, league;
        public ImageView removeButton, rankImage;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            league = (TextView) view.findViewById(R.id.league);
            removeButton = (ImageView) view.findViewById(R.id.deleteButton);
            rankImage = (ImageView) view.findViewById(R.id.rankImage);
        }
    }

    public LeagueAdapter(List<LeagueInfo> leagueInfoList, OnPlayerDeleted onPlayerDeleted) {
        this.leagueInfoList = leagueInfoList;
        this.onPlayerDeleted= onPlayerDeleted;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.league_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        LeagueInfo leagueInfo = leagueInfoList.get(position);
        holder.name.setText(leagueInfo.getPlayerOrTeamName());
        holder.league.setText(leagueInfo.getTier()+" "+leagueInfo.getRank()+" "+leagueInfo.getLeaguePoints()+"lp");
        Context context = holder.rankImage.getContext();
       // int id = context.getResources().getIdentifier(leagueInfo.getTier()+"_"+leagueInfo.getRank(), "drawable", context.getPackageName());

        String liga = leagueInfo.getTier().toLowerCase()+"_"+leagueInfo.getRank().toLowerCase();
        int id = context.getResources().getIdentifier(liga, "drawable", context.getPackageName());
        holder.rankImage.setImageResource(id);
        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlayerDeleted.onPlayerDeleted(leagueInfoList.get(position));
                leagueInfoList.remove(position);
                notifyDataSetChanged();







            }
        });
    }



    @Override
    public int getItemCount() {
        return leagueInfoList.size();
    }
}
