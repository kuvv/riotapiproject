package com.kuvv.riotapiproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuvv on 05.08.2017.
 */

public class DatabaseProvider {

    private DatabaseHelper databaseHelper;
    private Context context;

    public DatabaseProvider(Context context) {
        this.context = context;
        databaseHelper=new DatabaseHelper(context);
    }

    public void insertPlayer(Player player){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PlayersContract.Columns.PLAYER_ID, player.getId());
        values.put(PlayersContract.Columns.PLAYER_NAME, player.getName());
        db.insert(PlayersContract.TABLE_NAME,null,values);
    }

    public List<Player> getPlayersList() {
        List<Player> playersList = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        Cursor query = db.rawQuery("SELECT * FROM players;", null);
        if (query.moveToFirst()) {
            do {
                String name = query.getString(0);
                long id = query.getLong(1);
                playersList.add(new Player(name, id));
            } while (query.moveToNext());

        }
        query.close();
        return playersList;
    }


        public void deletePlayer(String name){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        String selection = PlayersContract.Columns.PLAYER_NAME + " LIKE ?";
        String[] selectionArgs = { name};
        db.delete(PlayersContract.TABLE_NAME, selection, selectionArgs);
    }
}






