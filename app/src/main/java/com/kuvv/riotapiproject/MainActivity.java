package com.kuvv.riotapiproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kuvv.riotapiproject.Constants.API_KEY;
import static com.kuvv.riotapiproject.Constants.API_URL;
import static com.kuvv.riotapiproject.Constants.ID_PATH;
import static com.kuvv.riotapiproject.Constants.LEAGUE_PATH;

public class MainActivity extends AppCompatActivity implements OnPlayerDeleted {


    List<Player> players;
    List<LeagueInfo> leaguesList;
    private RecyclerView recyclerView;
    private LeagueAdapter mAdapter;
    DatabaseProvider databaseProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        databaseProvider = new DatabaseProvider(getBaseContext());
        players = new ArrayList<>();
        leaguesList = new ArrayList<>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new LeagueAdapter(leaguesList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        //getPlayerID("kuvv");

//        addPlayer("junglefeed");

        getPlayersList();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //addPlayer("lb on faker");
                //Intent i = new Intent(MainActivity.this,AddPlayerPopup.class);
                startActivityForResult(new Intent(getApplicationContext(), AddPlayerPopup.class), 111);
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });


    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 111 && resultCode == RESULT_OK) {
            addPlayer(data.getStringExtra("name"));
        }
    }

    private void getPlayersList() {
        players = databaseProvider.getPlayersList();
//            SQLiteDatabase sqLiteDatabase= getBaseContext().openOrCreateDatabase("RiotApiProject.db", MODE_PRIVATE, null);
//            String sql ="CREATE TABLE IF NOT EXISTS players(_id INTEGER, id LONG PRIMARY KEY, name TEXT);";
//            sqLiteDatabase.execSQL(sql);
//            Cursor query = sqLiteDatabase.rawQuery("SELECT * FROM players;", null);
//            if(query.moveToFirst()){
//                do{
//                    String name = query.getString(0);
//                    long id = query.getLong(1);
//                    players.add(new Player(name,id));
//                } while(query.moveToNext());
//
//            }
//            query.close();
//            sqLiteDatabase.close();
        for (Player i : players) {
            getLeagueInfo(i.getId());

        }

        Log.e("league info added", leaguesList.toString());
    }


    private void getPlayerID(String name) {

        String URL = new String(API_URL + ID_PATH + name + "/");

        APICalls retrofit = RetrofitClient.getClient().create(APICalls.class);
        Call<Player> call = retrofit.getID(URL, API_KEY);
        call.enqueue(new Callback<Player>() {

            @Override
            public void onResponse(Call<Player> call, Response<Player> response) {
                if (response.isSuccessful()) {

                }


                //Player player =response.body();
            }

            @Override
            public void onFailure(Call<Player> call, Throwable t) {

            }
        });

    }

    private void addPlayer(String name) {

        String URL = new String(API_URL + ID_PATH + name + "/");

        APICalls retrofit = RetrofitClient.getClient().create(APICalls.class);
        Call<Player> call = retrofit.getID(URL, API_KEY);
        call.enqueue(new Callback<Player>() {

            @Override
            public void onResponse(Call<Player> call, Response<Player> response) {
                switch (response.code()) {
                    case 200:
//                        SQLiteDatabase sqLiteDatabase = getBaseContext().openOrCreateDatabase("RiotApiProject.db", MODE_PRIVATE, null);
//                        String sql = "CREATE TABLE IF NOT EXISTS players(name TEXT, id LONG PRIMARY KEY);";
//                        sqLiteDatabase.execSQL(sql);
//                        sql = "INSERT OR REPLACE INTO players VALUES('" + response.body().getName() + "'," + response.body().getId() + ");";
//                        sqLiteDatabase.execSQL(sql);
//                        sqLiteDatabase.close();

                        databaseProvider.insertPlayer(new Player(response.body().getName(), response.body().getId()));
                        //players.add(new Player(response.body().getName(),response.body().getId()));
                        getLeagueInfo(response.body().getId());
                        mAdapter.notifyDataSetChanged();
                        break;
                    case 403:
                        Toast.makeText(MainActivity.this, "API key expired! ", Toast.LENGTH_LONG).show();
                        break;

                    case 404:
                        Toast.makeText(MainActivity.this, "Player not found", Toast.LENGTH_LONG).show();
                        break;


                    //Player player =response.body();

                }
            }


            @Override
            public void onFailure(Call<Player> call, Throwable t) {

            }
        });

    }


    private void getLeagueInfo(long id) {

        String URL = new String(API_URL + LEAGUE_PATH + id + "/");

        APICalls retrofit = RetrofitClient.getClient().create(APICalls.class);
        Call<List<LeagueInfo>> call = retrofit.getLeague(URL, API_KEY);
        call.enqueue(new Callback<List<LeagueInfo>>() {

            @Override
            public void onResponse(Call<List<LeagueInfo>> call, Response<List<LeagueInfo>> response) {
                switch (response.code()) {
                    case 200:
                        for (LeagueInfo info : response.body()) {
                            if (info.getQueueType().equals("RANKED_SOLO_5x5")) {
                                leaguesList.add(info);
                                mAdapter.notifyDataSetChanged();

                            }
                        }
                        break;
                    case 403:
                        Toast.makeText(MainActivity.this, "API key expired! ", Toast.LENGTH_LONG).show();
                        break;

                }
                //   Player player =new Player();
            }

            @Override
            public void onFailure(Call<List<LeagueInfo>> call, Throwable t) {
                Log.e("nieposzlo", "nieposzlo");
            }
        });
    }


    @Override
    public void onPlayerDeleted(LeagueInfo leagueInfo) {
//        SQLiteDatabase sqLiteDatabase = getBaseContext().openOrCreateDatabase("RiotApiProject.db", MODE_PRIVATE, null);
//        String sql = "DELETE FROM players WHERE name='"+leagueInfo.getPlayerOrTeamName()+"';";
//        sqLiteDatabase.execSQL(sql);
//        sqLiteDatabase.close();
        databaseProvider.deletePlayer(leagueInfo.getPlayerOrTeamName());


    }
}
