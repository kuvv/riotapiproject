package com.kuvv.riotapiproject;

import android.provider.BaseColumns;

/**
 * Created by kuvv on 16.07.2017.
 */

public class Constants {


    private Constants(){
    }

    public static final String API_KEY = "RGAPI-46057fea-956f-40ad-85f0-e85b8169a9f2";
    //public static final String API_URL = "https://eun1.api.riotgames.com/lol";
    public static final String API_URL = "https://eun1.api.riotgames.com/lol/";
    public static final String ID_PATH = "summoner/v3/summoners/by-name/";
    public static final String LEAGUE_PATH = "league/v3/positions/by-summoner/";

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";
    }
}
