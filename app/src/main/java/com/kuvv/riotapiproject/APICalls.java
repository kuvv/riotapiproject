package com.kuvv.riotapiproject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by kuvv on 16.07.2017.
 */

public interface APICalls {

    //@GET("lol/summoner/v3/summoners/by-name/")
    @GET
    Call<Player> getID(@Url String Url, @Query("api_key") String API_KEY);
    //Call<Player> getID( @Query("api_key") String API_KEY);
    @GET
    //Call<List<LeagueInfo>> getLeague(@Url String Url, @Query("api_key") String API_KEY);
    Call<List<LeagueInfo>> getLeague(@Url String Url, @Query("api_key") String API_KEY);


}
