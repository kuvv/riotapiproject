package com.kuvv.riotapiproject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kuvv on 16.07.2017.
 */

public class Player {
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
