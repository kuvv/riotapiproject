package com.kuvv.riotapiproject;

/**
 * Created by kuvv on 27.07.2017.
 */

public interface OnPlayerDeleted {
    void onPlayerDeleted(LeagueInfo leagueInfo);
}
